<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pass = Hash::make('admin999');
        $user = User::create(
            [
                "username" => "admin999",
                "email" => "admin999@admin.com",
                "password" => $pass,
                "phone" => "0978785678",
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ]
        );

        $user->roles()->attach([1, 2]);
    }
}
