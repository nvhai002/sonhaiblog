<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(
            [
                "name_role" => "admin",
                "describ" => "full permission admin"
            ], [
                "name_role" => "blog_member",
                "describ" => "blog member"
            ]
        );
    }
}
