<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(
            [
                [
                    "name"    => "Kpop",
                    "describ" => "Somethings you like ...",
                    "status" => 1,
                ],
                [
                    "name"    => "Backend Web",
                    "describ" => "Somethings you like ...",
                    "status" => 1,
                ],
                [
                    "name"    => "Frontend Web",
                    "describ" => "Somethings you like ...",
                    "status" => 1,
                ]
            ]

        );
    }
}
