<?php

Route::namespace('Admin')->prefix('admin')->group(function () {
    Route::group(['middleware'=>'notlogin'], function () {
        Route::get('login','AuthController@login');
        Route::post('postlogin','AuthController@postLogin');
    });
    Route::get('logout', 'AuthController@logout');
    Route::group(['middleware' => ['checklogin']], function () {
        Route::get('/', 'HomeController@index');
        //slider CRUD
        Route::get('/category', 'CategoryController@index');
        Route::get('/category/add', 'CategoryController@add');
        Route::post('/category/postadd', 'CategoryController@postAdd');
        Route::get('/category/edit/{id}', 'CategoryController@edit');
        Route::post('/category/postedit/{id}', 'CategoryController@postEdit');
        Route::get('/category/delete/{id}', 'CategoryController@delete');
    });
});



