<?php

Route::get('/home', 'HomeController@index');
Route::get('/', 'HomeController@index');

Route::group(["middleware" => "notsignin"], function () {
    //đăng ký tài khoản
    Route::get('/register', 'AuthController@register');
    Route::post('/post_register', 'AuthController@postRegister');
    Route::get('/signin', 'AuthController@signin');
//    Route::get('/signin', 'Auth\LoginController@showLoginForm');
    Route::post('/post_signin', 'AuthController@postSignin');
//    Route::post('/post_signin', 'Auth\LoginController@login');
});
Route::get('/signout', 'AuthController@signout');
//Route::get('/singout', 'Auth\LoginController@logout');

Route::group(["middleware" => "checksignin"], function () {
    Route::get('/add-post', 'PostController@addPost');
    Route::post('/add-post', 'PostController@postAddPost');
    Route::get('/edit-post/{id}', 'PostController@editPost');
    Route::post('/edit-post/{id}', 'PostController@postEditPost');
    Route::get('/post', 'PostController@index');
    Route::get('/my-post', 'PostController@getMyPost');
    Route::get('/post-detail/{id}', 'PostController@getPostDetail');
    Route::get('/del-post/{id}', 'PostController@delPost');

});

Route::post('/get-category', 'PostController@getCategory');

?>
