<?php

define('STATUS', [
   1 => 'Processing',
   2 => 'Complete'
]);

define('VISIBLE_LEVEL', [
    1 => 'Public',
    2 => 'Private'
]);

define('TYPE', [
    1 => 'Post',
    2 => 'Answer'
]);
