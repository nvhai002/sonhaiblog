@extends('layouts/master')
@section('content')
    <div id="content">

        <div id="inner-content">

            <div class="blog-block">
                <p>
                    Đăng nhập
                </p>
                {!! Form::open(['url'=>'post_signin','method'=>'post','id'=>'signin_form']) !!}
                <div class="form-group">
                    {!! Form::label('credential','Tài khoản') !!}
                    {!! Form::text('credential','',
                                    ['class'=>'form-control',
                                     'placeholder'=>'Nhập tên tài khoản hoặc email'
                                    ])
                     !!}
                </div>
                <div class="form-group">
                    {!! Form::label('password','Mật khẩu') !!}
                    {!! Form::password('password',
                                    ['class'=>'form-control',
                                     'placeholder'=>'Nhập mật khẩu'
                                    ])
                     !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Đăng nhập',
                            [ 'class'=>'btn btn-primary'
                            ]
                     )
                     !!}
                    <p >Chưa có tài khoản ? <a href="register" class="text-danger">Tạo ngay !</a> </p>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <!-- if you plan on using a left aligned sidebar put the sidebar div just before inner-content
        div and have a look in general.css to move a margin property from #inner-content to #sidebar -->
        @include('layouts/sidebar')
        @include('layouts/footer')

    </div>
    <script src="validate/loginvalidate.js"></script>
@endsection
