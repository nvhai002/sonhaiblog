@extends('layouts/master')
@section('content')
    <div id="content">

        <div id="inner-content">

            <div class="blog-block">
                <p>
                    Đăng ký tài khoản
                </p>
                {!! Form::open(['url'=>'post_register','method'=>'post','id'=>'register_form']) !!}
                <div class="form-group">
                    {!! Form::label('username','Tên tài khoản') !!}
                    {!! Form::text('username','',
                                    ['class'=>'form-control',
                                     'placeholder'=>'Nhập tên tài khoản'
                                    ])
                    !!}
                    @if($errors->has('username'))
                        <span class="text text-danger">
                        <label class="error" for="username"
                               style="">{{$errors->first('username')}}
                        </label>
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    {!! Form::label('email','Email') !!}
                    {!! Form::email('email','',
                                    ['class'=>'form-control',
                                     'placeholder'=>'Nhập email'
                                    ])
                     !!}
                    @if($errors->has('email'))
                        <span class="text text-danger">
                        <label class="error" for="email"
                               style="">{{$errors->first('email')}}
                        </label>
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    {!! Form::label('password','Mật khẩu') !!}
                    {!! Form::password('password',
                                    ['class'=>'form-control',
                                     'placeholder'=>'Nhập mật khẩu'
                                    ])
                     !!}
                    @if($errors->has('password'))
                        <span class="text text-danger">
                        <label class="error" for="password"
                               style="">{{$errors->first('password')}}
                        </label>
                    </span>
                    @endif
                </div>

                <div class="form-group">
                    {!! Form::label('repassword','Nhập Lại Mật khẩu') !!}
                    {!! Form::password('repassword',
                                    ['class'=>'form-control',
                                     'placeholder'=>'Nhập lại mật khẩu'
                                    ])
                    !!}
                </div>
                <div class="form-group">
                    {!! Form::label('phone','số điện thoại') !!}
                    {!! Form::text('phone','',
                                    ['class'=>'form-control',
                                     'placeholder'=>'Nhập số điện thoại'
                                    ])
                     !!}
                    @if($errors->has('phone'))
                        <span class="text text-danger">
                        <label class="error" for="phone"
                               style="">{{$errors->first('phone')}}
                        </label>
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    {!! Form::submit('Đăng ký',
                            [ 'class'=>'btn btn-primary'
                            ]
                                    )
                     !!}
                </div>
                {!! Form::close() !!}

            </div>

        </div>
        <!-- if you plan on using a left aligned sidebar put the sidebar div just before inner-content
        div and have a look in general.css to move a margin property from #inner-content to #sidebar -->
        @include('layouts/sidebar')
        @include('layouts/footer')

    </div>
    <script src="validate/loginvalidate.js"></script>
@endsection
