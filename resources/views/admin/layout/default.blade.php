<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="{{asset('')}}">
    <title>Pike Admin - Free Bootstrap 4 Admin Template</title>
    <meta name="description" content="Free Bootstrap 4 Admin Theme | Pike Admin">
    <meta name="author" content="Pike Web Development - https://www.pikephp.com">

    <!-- Favicon -->
    <link rel="shortcut icon" href="adminxxx/assets/images/favicon.ico">

    <!-- Bootstrap CSS -->
    <link href="adminxxx/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- Font Awesome CSS -->
    <link href="adminxxx/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!-- Custom CSS -->
    <link href="adminxxx/assets/css/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="sweetalert2.min.css">
    <!-- BEGIN CSS for this page -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"/>
    <!-- END CSS for this page -->

</head>

<body class="adminbody">

<div id="main">

    <!-- top bar navigation -->
    @include('admin/layout/headerbar')
    <!-- End Navigation -->


    <!-- Left Sidebar -->
    @include('admin/layout/left_sidebar')

<!-- End Sidebar -->

    @yield('content')

    <!-- END content-page -->
    @include('admin/layout/footer')


</div>
<!-- END main -->

@include('admin/layout/script')
<!-- END Java Script for this page -->

</body>
</html>