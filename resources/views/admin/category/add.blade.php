@extends('admin/layout/default')
@section('content')
    <div class="content-page">

        <!-- Start content -->
        <div class="content">

            <div class="container-fluid">


                <div class="row">
                    <div class="col-xl-12">
                        <div class="breadcrumb-holder">
                            <h1 class="main-title float-left">Thêm mới danh mục</h1>
                            <ol class="breadcrumb float-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Thêm danh mục/li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {{--<div class="alert alert-danger" role="alert">--}}
                    {{--<h4 class="alert-heading">Parsley JavaScript form validation library</h4>--}}
                    {{--<p>You can find examples and documentation about Parsley form validation library here: <a target="_blank" href="http://parsleyjs.org/">Parsley documentation</a></p>--}}
                {{--</div>--}}


                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <div class="card mb-3">
                            <div class="card-header">
                                <h3><i class="fa fa-hand-pointer-o"></i> Thêm mới danh mục</h3>
                                A simple demo form that uses most of supported Parsley elements to show how to bind, configure and validate them properly.
                            </div>

                            <div class="card-body">
                                {!! Form::open(['url'=>'admin/category/postadd','method'=>'post']) !!}
                                {{--<form action="#" data-parsley-validate="" novalidate="">--}}
                                {!! Form::token() !!}
                                    <div class="form-group">
                                        {!! Form::label('name','Danh mục') !!}
                                        {!! Form::text('name','',
                                                        ['class'=>'form-control',
                                                         'placeholder'=>'Nhập tên danh mục'
                                                        ])
                                         !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('parent_id','Danh mục cha') !!}
                                        {!! Form::select('parent_category',
                                                         [1,2],
                                                        null,
                                                        ['class'=>'form-control',
                                                        'placeholder' => 'chọn danh mục cha'])
                                         !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('describ','Mô tả') !!}
                                        {!! Form::textarea('describ',null,['class'=>'form-control'
                                                                    ])
                                         !!}
                                    </div>
                                    <div class="form-group">
                                        <div class="checkbox">
                                            {!! Form::label('status') !!}
                                            {!! Form::checkbox('status','1',1) !!}
                                        </div>
                                    </div>

                                    <div class="form-group text-right m-b-0">
                                        <button class="btn btn-primary" type="submit">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary m-l-5">
                                            Cancel
                                        </button>
                                    </div>

                                {!! Form::close() !!}

                            </div>
                        </div><!-- end card-->
                    </div>






                </div>





            </div>
            <!-- END container-fluid -->

        </div>
        <!-- END content -->

    </div>
    @endsection