@extends('admin/layout/default')
@section('content')
    <div class="content-page">

        <!-- Start content -->
        <div class="content">

            <div class="container-fluid">



                <div class="row">
                    <div class="col-xl-12">
                        <div class="breadcrumb-holder">
                            <h1 class="main-title float-left">Tables</h1>
                            <ol class="breadcrumb float-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Tables</li>
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end row -->



                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <div class="card mb-3">
                            <div class="card-header">
                                <h3><i class="fa fa-table"></i> Basic table</h3>
                                Documentation and examples for opt-in styling of tables (given their prevelant use in JavaScript plugins) with Bootstrap 4 can be found <a target="_blank" href="https://getbootstrap.com/docs/4.0/content/tables/">here</a>
                            </div>

                            <div class="card-body">

                                <table class="table table-responsive-xl" id="example1">
                                    <thead>
                                    <tr>
                                        <th class="site_name" scope="col">Id</th>
                                        <th scope="col">First Name</th>
                                        <th scope="col">Last Name</th>
                                        <th scope="col">Username</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>sonhai</th>
                                            <th>sonhai</th>
                                            <th>sonhai</th>
                                            <th>sonhai</th>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div><!-- end card-->
                    </div>
                </div>
            </div>
            <!-- END container-fluid -->
        </div>
        <!-- END content -->

    </div>
@endsection