@extends('layouts/master')
@section('content')
    <div id="content">

        <div id="inner-content">

            <div class="blog-block">
                <h2>
                    {{$data->title}}
                </h2>
                <div class="blog-info">
                    <span>{{ date('d-M-Y',strtotime($data->created_at))}}</span>&nbsp;&nbsp;|&nbsp;&nbsp;
                    <a href="#">showcases</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#">27 comments</a>
                </div>
                <div>
                    {!! $data->content !!}
                </div>
            </div>
            <div class="bd-line"></div>
            <form action="#" method="post">
                <fieldset class="blog-comment-fieldset">
                    <h2><cufon class="cufon cufon-canvas" alt="Leave " style="width: 54px; height: 20px;"><canvas width="72" height="23" style="width: 72px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>Leave </cufontext></cufon><cufon class="cufon cufon-canvas" alt="a " style="width: 15px; height: 20px;"><canvas width="33" height="23" style="width: 33px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>a </cufontext></cufon><cufon class="cufon cufon-canvas" alt="comment" style="width: 82px; height: 20px;"><canvas width="96" height="23" style="width: 96px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>comment</cufontext></cufon></h2>
                    <ul>
                        <li>
                            <div class="cinput-bg"><input type="text" id="bc-name" name="bc_name" class="bc-input"></div><label for="bc-name">Name</label>
                        </li>
                        <li>
                            <div class="cinput-bg"><input type="text" id="bc-email" name="bc_email" class="bc-input"></div><label for="bc-email">Email</label>
                        </li>
                        <li>
                            <div class="cinput-bg"><input type="text" id="bc-website" name="bc_website" class="bc-input"></div>
                            <label for="bc-website">Website</label>
                        </li>
                        <li>
                            <div class="ctxtarea-bg"><textarea rows="6" cols="54" name="bc_textarea" class="bc-textarea"></textarea></div>
                        </li>
                        <li><div class="form-button"><span><input type="submit" value="&nbsp;&nbsp;add comment&nbsp;&nbsp;" name="go"></span></div></li>
                    </ul>
                </fieldset>
            </form>
        </div>
        <!-- if you plan on using a left aligned sidebar put the sidebar div just before inner-content
        div and have a look in general.css to move a margin property from #inner-content to #sidebar -->
        <div id="sidebar">

            <div class="box">
                <h2><cufon class="cufon cufon-canvas" alt="Categories" style="width: 91px; height: 20px;"><canvas width="105" height="23" style="width: 105px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>Categories</cufontext></cufon></h2>
                <div class="box-top"></div>
                <div class="box-middle">
                    <ul class="box-list">
                        <li><a href="#">Designing Techniques</a></li>
                        <li class="bl-active"><a href="#">Showcases</a></li>
                        <li><a href="#">Geotechnical Engineering</a></li>
                        <li><a href="#">Project Documentation</a></li>
                        <li><a href="#">Rehabilitations</a></li>
                    </ul>
                </div>
                <div class="box-bottom"></div>
            </div>

            <div class="box">
                <h2><cufon class="cufon cufon-canvas" alt="Archives " style="width: 77px; height: 20px;"><canvas width="95" height="23" style="width: 95px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>Archives </cufontext></cufon><cufon class="cufon cufon-canvas" alt="by " style="width: 26px; height: 20px;"><canvas width="43" height="23" style="width: 43px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>by </cufontext></cufon><cufon class="cufon cufon-canvas" alt="month" style="width: 56px; height: 20px;"><canvas width="68" height="23" style="width: 68px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>month</cufontext></cufon></h2>
                <div class="box-top"></div>
                <div class="box-middle">
                    <ul class="box-list">
                        <li><a href="#">January 2010</a></li>
                        <li class="bl-active"><a href="#">December 2009</a></li>
                        <li><a href="#">November 2009</a></li>
                        <li><a href="#">October 2009</a></li>
                    </ul>
                </div>
                <div class="box-bottom"></div>
            </div>

            <div class="box">
                <h2><cufon class="cufon cufon-canvas" alt="About " style="width: 57px; height: 20px;"><canvas width="74" height="23" style="width: 74px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>About </cufontext></cufon><cufon class="cufon cufon-canvas" alt="us" style="width: 20px; height: 20px;"><canvas width="34" height="23" style="width: 34px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>us</cufontext></cufon></h2>
                <div class="box-top"></div>
                <div class="box-middle">
                    <div class="box-content">
                        <p>
                            Discere legendos mediocritatem pri ut, quod tale delectus quo et, ei has agam vide probatus.
                            Eum latine iuvaret noluisse ea, eavo ei causae commune scaevola.
                        </p>
                        <p>
                            Etiam dicunt iuvaret est ei, omittam detraxit vulputate vim et, id inani iriure sanctus vis.
                        </p>
                        <a href="#" class="button"><span>read more</span></a>
                    </div>
                </div>
                <div class="box-bottom"></div>
            </div>

        </div>


    </div>

@endsection
