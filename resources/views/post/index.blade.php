@extends('layouts/master')
@section('content')
    <div id="content">

        <div id="inner-content">
            <ul class="news-list">
                @foreach($data as $post)
                    <li>
                        <div class="news-date">{{ date('d-M-Y',strtotime($post->created_at))}}</div>
                        <a href="post-detail/{{$post->id}}" class="news-item">{{str_limit($post->title,100)}}</a>
                        @if(Auth::user()->id == $post->owner_id)
                        <a href="edit-post/{{$post->id}}" class="news-icon">
                            <i class="fas fa-cloud"></i>
                        </a>
                            @endif
                    </li>
                @endforeach
            </ul>
            <div class="row">
            </div>

            <div class="pagination">
                <ul>
                </ul>
            </div>
        </div>
        <!-- if you plan on using a left aligned sidebar put the sidebar div just before inner-content
        div and have a look in general.css to move a margin property from #inner-content to #sidebar -->
        <div id="sidebar">

            <div class="box">
                <h2>
                    <cufon class="cufon cufon-canvas" alt="Quick " style="width: 54px; height: 20px;">
                        <canvas width="72" height="23"
                                style="width: 72px; height: 23px; top: -3px; left: -2px;"></canvas>
                        <cufontext>Quick</cufontext>
                    </cufon>
                    <cufon class="cufon cufon-canvas" alt="navigation" style="width: 89px; height: 20px;">
                        <canvas width="101" height="23"
                                style="width: 101px; height: 23px; top: -3px; left: -2px;"></canvas>
                        <cufontext>navigation</cufontext>
                    </cufon>
                </h2>
                <div class="box-top"></div>
                <div class="box-middle">
                    <ul class="box-list">
                        <li><a href="#">About us</a></li>
                        <li class="bl-active"><a href="#">Our mission</a></li>
                        <li><a href="#">Our philosophy</a></li>
                        <li><a href="#">Statistics</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </div>
                <div class="box-bottom"></div>
            </div>

            <div class="box">
                <h2>
                    <cufon class="cufon cufon-canvas" alt="About " style="width: 57px; height: 20px;">
                        <canvas width="74" height="23"
                                style="width: 74px; height: 23px; top: -3px; left: -2px;"></canvas>
                        <cufontext>About</cufontext>
                    </cufon>
                    <cufon class="cufon cufon-canvas" alt="us" style="width: 20px; height: 20px;">
                        <canvas width="34" height="23"
                                style="width: 34px; height: 23px; top: -3px; left: -2px;"></canvas>
                        <cufontext>us</cufontext>
                    </cufon>
                </h2>
                <div class="box-top"></div>
                <div class="box-middle">
                    <div class="box-content">
                        <p>
                            Discere legendos mediocritatem pri ut, quod tale delectus quo et, ei has agam vide probatus.
                            Eum latine iuvaret noluisse ea, eavo ei causae commune scaevola.
                        </p>
                        <p>
                            Etiam dicunt iuvaret est ei, omittam detraxit vulputate vim et, id inani iriure sanctus vis.
                        </p>
                        <a href="#" class="button"><span>read more</span></a>
                    </div>
                </div>
                <div class="box-bottom"></div>
            </div>

        </div>

        <div id="footer">
            <ul id="footer-menu">
                <li><a href="homepage.html">Home</a></li>
                <li><a href="about.html">About Us</a></li>
                <li><a href="services.html">Services</a></li>
                <li><a href="work.html">Our Work</a></li>
                <li><a href="bloglist.html">Blog</a></li>
                <li><a href="contact.php">Contact</a></li>
            </ul>

            <div class="footer-copyright">Copyright © 2009 <a href="#">Innova Construct</a>. All rights reserved.</div>
        </div>

    </div>

@endsection
