@extends('layouts/master')
@section('content')
    <div id="content">
        <div id="title">
            <h2>Create new post</h2>
        </div>
        <div class="form-style" style="">
            <div>
                @foreach ($errors->all() as $message)
                    <div class="text text-danger">{{$message}}</div>
                @endforeach
            </div>
            <form action="edit-post/{{$data->id}}" method="post">
                @csrf
                <input type="hidden" name="type" value="post" id="">
                <div class="form-row">
                    <div class="form-group col-6">
                        <label for="">Title</label>
                        {{ Form::text('title',$data->title,['class'=>'form-control'])}}
                    </div>
                    <div class="form-group col-2">
                        <label for="">Category</label>
                        {{ Form::select('category_id', $categoryArray,$data->category_id, [
                        'placeholde'=>'select category',
                        'class'=>'form-control'])}}

                    </div>
                    <div class="form-group col-2">
                        <label for="">Status</label>
                        {{ Form::select('status', STATUS, $data->status, ['class'=>'form-control'])}}
                    </div>
                    <div class="form-group col-2">
                        <label for="">Visibility Level</label>
                        {{ Form::select('visibility_level', VISIBLE_LEVEL,$data->visible_level,['class'=>'form-control'])}}
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-12">
                        {{ Form::textarea('contentxxx',$data->content,['class'=>'form-control'])}}
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        {{ Form::select('', $arrayTags, array_keys($arrayTags), [
                        'class' => 'form-control js-example-tags',
                        'multiple'=>'multiple'

                        ])}}
                        {{ Form::hidden('tags',implode(',',$arrayTags),['class'=>'form-control',
                                                   'id'=>'tag_value'
                         ])}}
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group">
                        <button class="btn btn-primary btn-md">Submit</button>
                    </div>
                    <div class="form-group" style="margin-left:15px">
                        <a href="/del-post/{{$data->id}}" class="btn btn-danger btn-md">Delete</a>
                    </div>
                </div>

                <script>
                    $('.js-example-tags').select2({
                        tags: true,
                        placeholder: "Select a tags"
                    });
                    $('.js-example-tags').change(function () {
                        data = $('.js-example-tags').val();
                        $('#tag_value').val(data);
                    });

                </script>
            </form>


        </div>
        <!-- if you plan on using a left aligned sidebar put the sidebar div just before inner-content
        div and have a look in general.css to move a margin property from #inner-content to #sidebar -->

    </div>

@endsection
