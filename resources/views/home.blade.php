@extends('layouts/master')
@section('content')
    <div id="content">

        <div id="inner-content">

            <div class="blog-block">
                <h2><a href="blogdetail.html"><cufon class="cufon cufon-canvas" alt="Tunneling " style="width: 87px; height: 20px;"><canvas width="105" height="23" style="width: 105px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>Tunneling </cufontext></cufon><cufon class="cufon cufon-canvas" alt="design " style="width: 60px; height: 20px;"><canvas width="78" height="23" style="width: 78px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>design </cufontext></cufon><cufon class="cufon cufon-canvas" alt="and " style="width: 37px; height: 20px;"><canvas width="54" height="23" style="width: 54px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>and </cufontext></cufon><cufon class="cufon cufon-canvas" alt="construction " style="width: 113px; height: 20px;"><canvas width="131" height="23" style="width: 131px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>construction </cufontext></cufon><cufon class="cufon cufon-canvas" alt="techniques" style="width: 94px; height: 20px;"><canvas width="108" height="23" style="width: 108px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>techniques</cufontext></cufon></a></h2>
                <div class="blog-info">
                    <span>20th January 2010</span>&nbsp;&nbsp;|&nbsp;&nbsp;
                    <a href="#">designing techniques</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#">6 comments</a>
                </div>
                <p>
                    Methods of tunneling vary with the nature of the material to be cut through. When soft earth is encountered, the excavation
                    is timbered for support as the <a href="#">work advances</a>; the timbers are sometimes left as a permanent lining for the tunnel.
                </p>
                <p>
                    Another method is to cut two <strong>parallel excavations</strong> in which the side walls are constructed first. Arches c
                    onnecting them are then built as the material between them is extracted.
                </p>
                <div class="rm-blog"><a href="blogdetail.html" class="button"><span>continue reading</span></a></div>
            </div>


            <div class="blog-block">
                <h2><a href="blogdetail.html"><cufon class="cufon cufon-canvas" alt="Gardens " style="width: 75px; height: 20px;"><canvas width="92" height="23" style="width: 92px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>Gardens </cufontext></cufon><cufon class="cufon cufon-canvas" alt="of " style="width: 23px; height: 20px;"><canvas width="40" height="23" style="width: 40px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>of </cufontext></cufon><cufon class="cufon cufon-canvas" alt="Eden " style="width: 47px; height: 20px;"><canvas width="65" height="23" style="width: 65px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>Eden </cufontext></cufon><cufon class="cufon cufon-canvas" alt="showcase" style="width: 83px; height: 20px;"><canvas width="96" height="23" style="width: 96px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>showcase</cufontext></cufon></a></h2>
                <div class="blog-info">
                    <span>11th January 2010</span>&nbsp;&nbsp;|&nbsp;&nbsp;
                    <a href="#">showcases</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#">27 comments</a>
                </div>
                <a href="blog/images/blog_img1.jpg" rel="lightbox" title="Gardens of Eden"><img src="blog/images/blog_img1.jpg" alt="" class="image blog-img"></a>
                <h3>Choosing the garden mystical location</h3>
                <p>
                    A garden is a planned space, usually outdoors, set aside for the display, cultivation, and enjoyment of plants and
                    other forms of nature. The garden can incorporate both natural and man-made materials. The most common form is known as a residential garden.
                </p>
                <p>
                    Some traditional types of eastern gardens, such as Zen gardens, use plants such as parsley.
                </p>
                <div class="rm-blog"><a href="blogdetail.html" class="button"><span>continue reading</span></a></div>
            </div>


            <div class="blog-block">
                <h2><a href="blogdetail.html"><cufon class="cufon cufon-canvas" alt="A " style="width: 18px; height: 20px;"><canvas width="35" height="23" style="width: 35px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>A </cufontext></cufon><cufon class="cufon cufon-canvas" alt="great " style="width: 49px; height: 20px;"><canvas width="67" height="23" style="width: 67px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>great </cufontext></cufon><cufon class="cufon cufon-canvas" alt="example " style="width: 77px; height: 20px;"><canvas width="94" height="23" style="width: 94px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>example </cufontext></cufon><cufon class="cufon cufon-canvas" alt="of " style="width: 23px; height: 20px;"><canvas width="40" height="23" style="width: 40px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>of </cufontext></cufon><cufon class="cufon cufon-canvas" alt="futuristic " style="width: 83px; height: 20px;"><canvas width="101" height="23" style="width: 101px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>futuristic </cufontext></cufon><cufon class="cufon cufon-canvas" alt="architecture: " style="width: 114px; height: 20px;"><canvas width="131" height="23" style="width: 131px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>architecture: </cufontext></cufon><cufon class="cufon cufon-canvas" alt="Golden " style="width: 66px; height: 20px;"><canvas width="83" height="23" style="width: 83px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>Golden </cufontext></cufon><cufon class="cufon cufon-canvas" alt="Works " style="width: 59px; height: 20px;"><canvas width="76" height="23" style="width: 76px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>Works </cufontext></cufon><cufon class="cufon cufon-canvas" alt="Headquarters " style="width: 120px; height: 20px;"><canvas width="138" height="23" style="width: 138px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>Headquarters </cufontext></cufon><cufon class="cufon cufon-canvas" alt="at " style="width: 23px; height: 20px;"><canvas width="41" height="23" style="width: 41px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>at </cufontext></cufon><cufon class="cufon cufon-canvas" alt="a " style="width: 15px; height: 20px;"><canvas width="33" height="23" style="width: 33px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>a </cufontext></cufon><cufon class="cufon cufon-canvas" alt="glance!" style="width: 62px; height: 20px;"><canvas width="78" height="23" style="width: 78px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>glance!</cufontext></cufon></a></h2>
                <div class="blog-info">
                    <span>8th January 2010</span>&nbsp;&nbsp;|&nbsp;&nbsp;
                    <a href="#">showcases</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#">34 comments</a>
                </div>
                <a href="blog/images/blog_img2.jpg" rel="lightbox" title="Golden Works Headquarters"><img src="blog/images/blog_img2.jpg" alt="" class="image blog-img"></a>
                <h3>Turning a dream into reality</h3>
                <p>
                    Methods of tunneling vary with the nature of the material to be cut through. When soft earth is encountered, the excavation is timbered for support as the work advances; the timbers are sometimes left as a permanent lining for the tunnel.
                </p>
                <p>
                    Another method is to cut two parallel excavations in which the side walls are constructed first. Arches connecting them are then built as the material between them is extracted.
                </p>
                <div class="rm-blog"><a href="blogdetail.html" class="button"><span>continue reading</span></a></div>
            </div>


            <div class="blog-block">
                <h2><a href="blogdetail.html"><cufon class="cufon cufon-canvas" alt="Deciding " style="width: 79px; height: 20px;"><canvas width="97" height="23" style="width: 97px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>Deciding </cufontext></cufon><cufon class="cufon cufon-canvas" alt="factors " style="width: 65px; height: 20px;"><canvas width="82" height="23" style="width: 82px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>factors </cufontext></cufon><cufon class="cufon cufon-canvas" alt="when " style="width: 52px; height: 20px;"><canvas width="69" height="23" style="width: 69px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>when </cufontext></cufon><cufon class="cufon cufon-canvas" alt="choosing " style="width: 81px; height: 20px;"><canvas width="98" height="23" style="width: 98px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>choosing </cufontext></cufon><cufon class="cufon cufon-canvas" alt="a " style="width: 15px; height: 20px;"><canvas width="33" height="23" style="width: 33px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>a </cufontext></cufon><cufon class="cufon cufon-canvas" alt="project " style="width: 65px; height: 20px;"><canvas width="82" height="23" style="width: 82px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>project </cufontext></cufon><cufon class="cufon cufon-canvas" alt="location" style="width: 69px; height: 20px;"><canvas width="81" height="23" style="width: 81px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>location</cufontext></cufon></a></h2>
                <div class="blog-info">
                    <span>22th December 2009</span>&nbsp;&nbsp;|&nbsp;&nbsp;
                    <a href="#">geotechnical engineering</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#">2 comments</a>
                </div>
                <p>
                    Geotechnical engineers perform geotechnical investigations to obtain information on the physical properties of soil
                    and rock underlying (and sometimes adjacent to) a site to design <a href="#">earthworks and foundations</a> for proposed structures,
                    and for repair of distress to earthworks and structures caused by subsurface conditions.
                </p>
                <h3>Geothechnical implications</h3>
                <p>
                    Geotechnical engineering is the branch of civil engineering concerned with the engineering behavior of earth materials.
                </p>
                <p>
                    Geotechnical engineering is the branch of <strong>civil engineering</strong> concerned with the engineering behavior of earth materials.
                </p>
                <div class="rm-blog"><a href="blogdetail.html" class="button"><span>continue reading</span></a></div>
            </div>

            <div class="pagination">
                <ul>
                    <li><a href="#" class="button"><span>1</span></a></li>
                    <li><a href="#" class="button active-pag"><span>2</span></a></li>
                    <li><a href="#" class="button"><span>3</span></a></li>
                    <li><a href="#" class="button"><span>4</span></a></li>
                </ul>
            </div>

        </div>
        <!-- if you plan on using a left aligned sidebar put the sidebar div just before inner-content
        div and have a look in general.css to move a margin property from #inner-content to #sidebar -->
        @include('layouts/sidebar')
        @include('layouts/footer')

    </div>
@endsection
