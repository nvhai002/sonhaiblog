<div id="hheader-wrapper">
    <div id="hheader">

        <ul class="home-slider"><!-- slider start -->
            <li>
                <a href="#"><img src="blog/images/tunnel_slide.jpg" width="630" height="281" alt="" class="slide-img" /></a>
                <div class="hs-desc">
                    <h1>The Grand Tunnel</h1>
                    <div class="slide-tagline">An innovative and cost effective road car tunnel</div>
                    <p>
                        A tunnel may be for pedestrians or cyclists, for general road traffic, for motor vehicles only, for rail traffic, or for a canal.
                    </p>
                    <p>
                        Before building a tunnel, detailed ground analyses and probe drills are made.
                    </p>
                    <a href="#" class="button"><span>read more</span></a>
                </div>
            </li>
            <li>
                <a href="#"><img src="blog/images/garden_slide.jpg" width="630" height="281" alt="" class="slide-img" /></a>
                <div class="hs-desc">
                    <h1>Gardens of Eden</h1>
                    <div class="slide-tagline">The long forgotten paradise is revived</div>
                    <p>
                        A garden is a planned space, usually outdoors, set aside for the display, cultivation, and enjoyment of
                        plants and other forms of nature. The garden can incorporate both natural and man-made materials.
                        The most common form is known as a residential garden.
                    </p>
                    <a href="#" class="button"><span>read more</span></a>
                </div>
            </li>

            <li>
                <a href="#"><img src="blog/images/tgv_slide.jpg" alt="" width="630" height="281" class="slide-img" /></a>
                <div class="hs-desc">
                    <h1>Train &agrave; Grande Vitesse</h1>
                    <div class="slide-tagline">Inter City Express</div>
                    <p>
                        The TGV (Train &agrave; Grande Vitesse, French for 'high-speed train') is France's high-speed rail service,
                        currently operated by VFE, the long-distance rail branch of SNCF, the French national rail operator.
                    </p>
                    <p>It was developed during the 1970s by GEC-Alsthom (now Alstom) and SNCF. </p>
                    <a href="#" class="button"><span>read more</span></a>
                </div>
            </li>

            <li>
                <a href="#"><img src="blog/images/future_slide.jpg" width="630" height="281" alt="" class="slide-img" /></a>
                <div class="hs-desc">
                    <h1>Future World</h1>
                    <div class="slide-tagline">And what you see is not the same</div>
                    <p>
                        Futurist architecture began as an early-20th century form of architecture characterized by anti-historicism
                        and long horizontal lines suggesting speed, motion and urgency.
                    </p>
                    <p>Probo mundi nostrum ei mel, vis graece impetus conclusionemque te.</p>
                    <a href="#" class="button"><span>read more</span></a>
                </div>
            </li><!-- slider end -->
        </ul>

    </div>
</div>
