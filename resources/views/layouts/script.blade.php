{{--<script type="text/javascript">Cufon.now();</script>--}}


@if(Session::has('msg_success'))
    <script>
        swal({
            position: 'top-mid',
            type: 'success',
            title: '{{ Session::get('msg_success') }}',
            showConfirmButton: true,
            timer: 3000
        })
    </script>
@endif
@if(Session::has('msg_fail'))
    <script>
        swal({
            type: 'error',
            title: 'Oops...',
            text: '{{ Session::get('msg_fail') }}',
            footer: '<a href>Why do I have this issue?</a>',
        })
    </script>
@endif
<script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.


    CKEDITOR.replace('contentxxx', {
        // Define the toolbar groups as it is a more accessible solution.
        toolbarGroups: [
            {
                "name": "basicstyles",
                "groups": ["basicstyles"]
            },
            {
                "name": "links",
                "groups": ["links"]
            },
            {
                "name": "paragraph",
                "groups": ["list", "blocks"]
            },
            {
                "name": "document",
                "groups": ["mode"]
            },
            {
                "name": "insert",
                "groups": ["insert"]
            },
            {
                "name": "styles",
                "groups": ["styles"]
            },

        ],
    });




</script>

<script>
    $(document).ready(
        $.ajax({
            type: 'POST',
            url: '/get-category',
            data: {
                name: "sonhai",
                job : "dev",
                _token: '{{ csrf_token() }}'
            },
            dataType: "json",
            success: function (data) {
                $.each(data, function( k, v ) {
                    $("ul#topic").append('<li><a href="work.html">'+v+'</a></li>');
                });
            }
        })
    );
</script>
