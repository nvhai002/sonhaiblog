<div id="header-wrapper">
    <div id="header">
        <div class="header-intro">
            <h1><cufon class="cufon cufon-canvas" alt="Our " style="width: 46px; height: 26px;"><canvas width="68" height="29" style="width: 68px; height: 29px; top: -3px; left: -3px;"></canvas><cufontext>Our </cufontext></cufon><cufon class="cufon cufon-canvas" alt="company's " style="width: 120px; height: 26px;"><canvas width="143" height="29" style="width: 143px; height: 29px; top: -3px; left: -3px;"></canvas><cufontext>company's </cufontext></cufon><cufon class="cufon cufon-canvas" alt="blog" style="width: 46px; height: 26px;"><canvas width="62" height="29" style="width: 62px; height: 29px; top: -3px; left: -3px;"></canvas><cufontext>blog</cufontext></cufon></h1>
            <p>
                Innova Construct proudly presents it’s blog! Here we share our passion with our readers of
                constructing majestic tunnels, buildings, gardens and a lot more!
            </p>
            <div class="rss"><a href="#"><img src="blog/images/rss_16.png" alt=""></a></div>
        </div>
        <div class="header-contact">
            <div class="hc-content">
                <h2><a href="contact.php"><cufon class="cufon cufon-canvas" alt="Contact " style="width: 75px; height: 20px;"><canvas width="92" height="23" style="width: 92px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>Contact </cufontext></cufon><cufon class="cufon cufon-canvas" alt="Us" style="width: 24px; height: 20px;"><canvas width="38" height="23" style="width: 38px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>Us</cufontext></cufon></a></h2>
                <p class="contact-tagline">Don't hesitate to send us a message!</p>
            </div>
            <img src="blog/images/contact_icon.png" alt="" class="hc-icon">
        </div>
    </div>
</div>