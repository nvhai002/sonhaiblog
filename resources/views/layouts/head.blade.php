<head>
    <base href="{{asset('')}}">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="google-site-verification" content="5RHtH0gdrdTHu8b2NKkzcBEWj0zdqOGlRGYuIiYC7Ts" />
    <link href="blog/css/index.css" type="text/css" rel="stylesheet" />
    <!--[if IE 6]>
    <script type="text/javascript" src="blog/js/dd_belated_png.js"></script>
    <script>DD_belatedPNG.fix('#logo a, .box-top, .box-bottom, .form-button, .form-button span, .pdf-icon, .header-contact');</script>

    <link href="blog/css/ie6.css" rel="stylesheet" type="text/css" />
    <![endif]-->
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">--}}
    <link rel="stylesheet" type="text/css" href="sweetalert2.min.css">
    <script src="sweetalert2.all.min.js"></script>

    <link rel="stylesheet" type="text/css" href="css/user_avatar.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="blog/css/form_style.css">

    <script type="text/javascript" src="blog/js/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="blog/js/cufon-yui.js"></script>
    <script type="text/javascript" src="blog/js/Anivers_400.font.js"></script>
    <script type="text/javascript" src="blog/js/jqueryslidemenu.js"></script>
    <script type="text/javascript" src="blog/js/jquery.cycle.js"></script>
    <script type="text/javascript" src="blog/js/slimbox2.js"></script>
    <script type="text/javascript" src="blog/js/custom.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="jquery.validate.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

    <script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>Fake Love</title>
</head>
