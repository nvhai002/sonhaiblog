<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Mirrored from goldenworks.eu/demos/innovaconstruct/white_red/homepage.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 21 Dec 2018 02:09:49 GMT -->
@include('layouts/head')
<body>
@include('layouts/top')

{{--@include('layouts/hheader-wrapper')--}}
@include('layouts/header-wrapper')

<div id="content-wrapper">
    @yield('content')
</div>
@include('layouts/script')


</body>
<div  class="footer-copyright">Copyright © 2009 <a href="#">Innova Construct</a>. All rights reserved.</div>
<!-- Mirrored from goldenworks.eu/demos/innovaconstruct/white_red/homepage.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 21 Dec 2018 02:10:03 GMT -->
</html>
