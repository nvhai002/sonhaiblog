<div id="sidebar">

    <div class="box">
        <h2><cufon class="cufon cufon-canvas" alt="Categories" style="width: 91px; height: 20px;"><canvas width="105" height="23" style="width: 105px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>Categories</cufontext></cufon></h2>
        <div class="box-top"></div>
        <div class="box-middle">
            <ul class="box-list">
                <li><a href="#">Designing Techniques</a></li>
                <li class="bl-active"><a href="#">Showcases</a></li>
                <li><a href="#">Geotechnical Engineering</a></li>
                <li><a href="#">Project Documentation</a></li>
                <li><a href="#">Rehabilitations</a></li>
            </ul>
        </div>
        <div class="box-bottom"></div>
    </div>

    <div class="box">
        <h2><cufon class="cufon cufon-canvas" alt="Archives " style="width: 77px; height: 20px;"><canvas width="95" height="23" style="width: 95px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>Archives </cufontext></cufon><cufon class="cufon cufon-canvas" alt="by " style="width: 26px; height: 20px;"><canvas width="43" height="23" style="width: 43px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>by </cufontext></cufon><cufon class="cufon cufon-canvas" alt="month" style="width: 56px; height: 20px;"><canvas width="68" height="23" style="width: 68px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>month</cufontext></cufon></h2>
        <div class="box-top"></div>
        <div class="box-middle">
            <ul class="box-list">
                <li><a href="#">January 2010</a></li>
                <li class="bl-active"><a href="#">December 2009</a></li>
                <li><a href="#">November 2009</a></li>
                <li><a href="#">October 2009</a></li>
            </ul>
        </div>
        <div class="box-bottom"></div>
    </div>

    <div class="box">
        <h2><cufon class="cufon cufon-canvas" alt="About " style="width: 57px; height: 20px;"><canvas width="74" height="23" style="width: 74px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>About </cufontext></cufon><cufon class="cufon cufon-canvas" alt="us" style="width: 20px; height: 20px;"><canvas width="34" height="23" style="width: 34px; height: 23px; top: -3px; left: -2px;"></canvas><cufontext>us</cufontext></cufon></h2>
        <div class="box-top"></div>
        <div class="box-middle">
            <div class="box-content">
                <p>
                    Discere legendos mediocritatem pri ut, quod tale delectus quo et, ei has agam vide probatus.
                    Eum latine iuvaret noluisse ea, ea mei causae commune scaevola.
                </p>
                <p>
                    Etiam dicunt iuvaret est ei, omittam detraxit vulputate vim et, id inani iriure sanctus vis.
                </p>
                <a href="#" class="button"><span>read more</span></a>
            </div>
        </div>
        <div class="box-bottom"></div>
    </div>

</div>
