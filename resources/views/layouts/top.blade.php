<div id="top">
    <div>
        <div id="logo"><a href="homepage.html">Innova Construct logo</a></div>

        <div id="search">
            <form action="#" method="post">
                <fieldset>
                    <div class="search-input-bg">
                        <input type="text" class="search-input" value="search" name="search"
                               onfocus="if(this.value=='search') {this.value='';}"
                               onblur="if(this.value=='') {this.value='search'}"/>
                    </div>
                    <div class="form-button"><span><input type="submit" value="GO" name="go"/></span></div>
                </fieldset>
            </form>
        </div>
    </div>

    <div id="myslidemenu" class="jqueryslidemenu">
        <ul class="primary-menu">
            <li><a href="#">Topic</a>
                <ul id="topic">
                </ul>
            </li>
            <li><a href="/post">Post</a></li>
            <li><a href="#">Question</a>
                <ul>
                    <li><a href="services.html">Services</a></li>
                    <li><a href="news.html">News Archive</a></li>
                    <li><a href="bloglist.html">Blog</a></li>
                    <li><a href="blogdetail.html">Blog detail</a></li>
                    <li><a href="about.html">About Us</a></li>
                    <li><a href="work.html">Our Work</a></li>
                    <li><a href="contact.html">Contact</a></li>
                    <li><a href="#">Just send -></a>
                        <ul>
                            <li><a href="#">me</a></li>
                            <li><a href="#">a sign</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="add-post">Ask Question</a></li>
            <li><a href="add-post">Write Post</a></li>
            @if(!Auth::check())
                <li>
                    <a href="signin">Login</a>
                </li>
            @else
                <li>
                    <a href="signin">
                        <img class="user_avatar" src="images/user_avatar/default_avatar.png" alt="">
                        {{Auth::user()->username}}
                    </a>
                    <ul>
                        <li>

                            <a href="services.html"><i class="fa fa-user"></i> My acount</a>
                        </li>
                        <li>
                            <a href="my-post"><i class="fa fa-book" aria-hidden="true"></i> My post</a>
                        </li>
                        <li>
                            <a href="signout"><i class="fa fa-sign-out"></i> Sign out</a>
                        </li>
                    </ul>
                </li>
            @endif
        </ul>
    </div>


</div>

