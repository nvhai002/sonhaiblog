@extends('layouts/master')
@section('content')
    <div id="content">

        <div id="inner-content">
            <h1>Welcome to our company</h1>
            <p>
                Ad admodum alienum euripidis per, ea detracto disputando duo. Tritani definiebas eum te. Vis consul constituto
                posidonium at, mea solet tation sadipscing no. Ad malorum ceteros percipitur duo, no alii eros copiosae ius,
                ius nostrud offendit <strong>accommodare</strong> at. Everti facilis singulis eam no, vis quaestio consetetur no.
            </p>
            <p>
                Equidem takimata mnesarchum no cum, habemus volutpat has an. Essent aperiri <a href="#">delicata</a> pri in.
                Has aeque integre antiopam no, his alii prima essent id.
            </p>

            <div class="featured"> <!-- start featured works -->
                <h2>Featured works</h2>
                <ul>
                    <li>
                        <a href="#"><img src="blog/images/fw_img1.jpg" alt="" class="fw-img" /></a>
                        <div class="fw-txt">
                            <h3><a href="#">Blue Moon Tunnel</a></h3>
                            <p>
                                Nisl ludus fuisset cu quo, ex usu appareat insolens invenire. No nisl omnes reformidans nec.
                                Eirmod timeam eam ne, at nusquam lucilius his.
                            </p>
                        </div>
                        <div class="decoration"></div>
                    </li>
                    <li>
                        <a href="#"><img src="blog/images/fw_img2.jpg" alt="" class="fw-img" /></a>
                        <div class="fw-txt">
                            <h3><a href="#">London Underground</a></h3>
                            <p>
                                Nisl ludus fuisset cu quo, ex usu appareat insolens invenire. No nisl omnes reformidans nec.
                                Eirmod timeam eam ne, at nusquam lucilius his.
                            </p>
                        </div>
                        <div class="decoration"></div>
                    </li>
                    <li>
                        <a href="#"><img src="blog/images/fw_img3.jpg" alt="" class="fw-img" /></a>
                        <div class="fw-txt">
                            <h3><a href="#">Innova Construct Office Buildings</a></h3>
                            <p>
                                Nisl ludus fuisset cu quo, ex usu appareat insolens invenire. No nisl omnes reformidans nec.
                                Eirmod timeam eam ne, at nusquam lucilius his.
                            </p>
                        </div>
                        <div class="decoration"></div>
                    </li>
                </ul>
            </div><!-- end featured works -->


        </div>
        <!-- if you plan on using a left aligned sidebar put the sidebar div just before inner-content
        div and have a look in general.css to move a margin property from #inner-content to #sidebar -->
        @include('layouts/sidebar')
        @include('layouts/footer')

    </div>
@endsection