$(document).ready(function(){
    $('#register_form').validate({
        rules:{
            username : "required",
            email : {
                required : true,
                email : true,
            },
            password : "required",
            repassword :{
                required:true,
                equalTo:"#password",
            },
            phone : {
                required : true,
                number : true,
                maxlength :11 ,
            },
        },
        messages:{
            username : {
                required : "Thông tin bắt buộc !",
            },
            email : {
                required : "Thông tin bắt buộc !",
                email : "Email nhập chưa đúng định dạng",
            },
            password : {
                required : "Thông tin bắt buộc !",
            },
            repassword : {
                required : "Thông tin bắt buộc !",
                equalTo : "password nhập lại chưa đúng",
            },
            phone : {
                required : "Thông tin bắt buộc !",
            }
        },
        errorPlacement: function(label, element) {
            label.addClass('text text-danger');
            label.insertAfter(element);
        },
        wrapper: 'span'
    })

    $('#signin_form').validate({
        rules:{
            credential : {
                required : true,
            },
            password : "required",
        },
        messages:{
            credential : {
                required : "Thông tin bắt buộc !",
            },
            password : {
                required : "Thông tin bắt buộc !",
            }
        },
        errorPlacement: function(label, element) {
            label.addClass('text text-danger');
            label.insertAfter(element);
        },
        wrapper: 'span'
    })
});