<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testLoginFailWithWrongUsername()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/admin/login')
                ->type('name', 'abcdessss')
                ->type('password', 'abcdessssss')
                ->press('LOGIN')
                ->assertTitle('Login V1')
                ->assertPathIs('/admin/login')
                ->waitForText('Thông tin đăng nhập không hợp lệ');
        });
    }
    //AutoTest_Loginout_02
    public function testLoginSuccessWithTrueUsername()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/admin/login')
                ->type('name', 'admin99jj9')
                ->type('password', 'admin999')
                ->press('LOGIN')
                ->assertTitle('Pike Admin - Free Bootstrap 4 Admin Template')
                ->assertPathIs('/admin')
                ->waitForText('Đăng nhâp thành công');
        });
    }
}
