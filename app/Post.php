<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{
    protected $table = "posts";

    protected $fillable
        = [
            'title',
            'content',
            'owner_id',
            'type',
            'view',
            'category_id',
            'status',
            'visibility_level'
        ];

    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'post_tag', 'post_id',
            'tag_id');
    }

    public function getListPostByOwner($userId)
    {
        $data = self::where([
            'type'     => 'post',
            'owner_id' => $userId
        ])->get();
        return $data;
    }

    public function getPostById($id)
    {
        $data = self::where([
            'type' => 'post',
            'id'   => $id
        ])
            ->first();
        return $data;
    }

    public function getTagsByPost($idPost)
    {
        $arrayTags = [];
        $postWithTags = self::where(['id' => $idPost])->select(['title', 'id'])
            ->with('tags')->first();
        foreach ($postWithTags->tags->keyBy('name') as $key => $value) {
            $arrayTags[$key] = $value->name;
        };
        return $arrayTags;
    }

    public function createPost($request)
    {
        $data = [
            'title'            => $request->title,
            'owner_id'         => Auth::user()->id,
            'content'          => $request->contentxxx,
            'type'             => $request->type,
            'category_id'      => $request->category_id,
            'status'           => $request->status,
            'visibility_level' => $request->visibility_level,
        ];

        return self::Create($data);
    }

    public function updatePost($id, $request)
    {
        $data = [
            'title'            => $request->title,
            'owner_id'         => Auth::user()->id,
            'content'          => $request->contentxxx,
            'type'             => $request->type,
            'category_id'      => $request->category_id,
            'status'           => $request->status,
            'visibility_level' => $request->visibility_level,
        ];
        self::find($id)->update($data);
    }

    public function checkTags($tags, $post_id)
    {
        $tagsArrayId = [];
        $post = $this->getPostById($post_id);
        if ($tags != '') {
            $tagsArray = explode(',', $tags);
            foreach ($tagsArray as $tag) {
                $tagObj = Tag::where('name', $tag)->first();
                if ($tagObj) {
                    array_push($tagsArrayId, $tagObj->id);
                } else {
                    $newTag = Tag::create(['name' => $tag]);
                    array_push($tagsArrayId, $newTag->id);
                }
            }
        }
        $post->tags()->sync($tagsArrayId);

    }

    public function delPost($id)
    {
        $post = $this->getPostById($id);
        $post->tags()->sync([]);
        $post->delete();

    }
}
