<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateRegister extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'username' => 'required|min:4|max:100|unique:users',
            'email' => 'required|min:4|max:100|unique:users',
            'password' => 'required|min:4|max:100',
            'phone' => 'required|min:4|max:100',
        ];
    }

    public function messages()
    {
        return [
            'username.required' => 'Phải nhập tên tài khoản',
            'username.min' => 'Tối thiểu 4 ký tự',
            'username.max' => 'Tối đa 100 ký tự',
        ];
    }
}
