<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class   ValidateEditPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:6|max:150|unique:posts,title,'.$this->id,
            'contentxxx' => 'required|min:6',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Tiêu đề không được để trống',
            'title.min' => 'Tối thiểu 6 ký tự',
            'title.unique' => 'Tiêu đề đã tồn tại',
            'contentxxx.max' => 'Tối  đa 50 ký tự',
            'content.required' => 'Nội dung không được để trống',
            'content.min' => 'Tối thiểu 6 ký tự',
        ];
    }
}
