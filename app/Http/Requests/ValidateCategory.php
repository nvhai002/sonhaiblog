<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:4|max:100|unique:categories',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Phải nhập tên danh mục',
            'name.min' => 'Tối thiểu 4 ký tự',
            'name.max' => 'Tối đa 100 ký tự',
        ];
    }
}
