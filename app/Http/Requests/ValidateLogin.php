<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateLogin extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:4|max:100',
            'password' => 'required|min:4|max:100',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Phải nhập username',
            'name.min' => 'Tối thiểu 4 ký tự',
            'name.max' => 'Tối đa 100 ký tự',
            'password.required' => 'Phải nhập mật khẩu',
            'password.min' => 'Mật khẩu tối thiểu 4 ký tự',
            'password.max' => 'Tối đa 100 ký tự',
        ];
    }
}
