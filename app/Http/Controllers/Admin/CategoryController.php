<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Requests\ValidateCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index()
    {
//        $Categorys = Category::Where('status','=',1)->get();
        $Categories = Category::paginate(10);
        return view('admin/Category/index',compact('Categories'));
    }

    public function add()
    {
        $category = Category::where('status',1)->get();
        return view('admin/Category/add',compact('category'));
    }

    public function postAdd(ValidateCategory $request)
    {
        $Category = new Category();
        $Category->name = $request->name;
        $Category->parent_id = (isset($request->parent_id))? $request->parent_id : 0;
        $Category->describ = $request->describ;
        $Category->status = (isset($request->status)) ? 1 : 0;
        $Category->save();
        return redirect('admin/category')->with('msg_add_success','Thêm thành công');
    }

    public function edit($id)
    {
        $Category = Category::findOrFail($id);
        return view('admin/Category/edit',compact('Category'));
    }

    public function postEdit(ValidateCategory $request,$id)
    {
        $Category = Category::findOrFail($id);
        $Category->name= $request->Category_name;
        $Category->status = (isset($request->status)) ? 1 : 0;
        $Category->save();
        return redirect('admin/Category')->with('msg_add_success','Cập nhật thành công');
    }

    public function delete($id)
    {
        Category::destroy($id);
        return redirect('admin/Category')->with('msg_add_success','Xóa thành công');
    }
}
