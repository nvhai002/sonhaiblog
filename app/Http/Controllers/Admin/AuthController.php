<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ValidateLogin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login()
    {
        return view('admin/login/login');
    }

    public function postLogin(ValidateLogin $request)
    {
        if (Auth::attempt(['name' => $request->name, 'password' => $request->password])) {
            return redirect('admin')->with('msg_success', 'Đăng nhâp thành công');
        }
        return redirect('admin/login')->with('msg_fail', 'Thông tin đăng nhập không hợp lệ');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('admin/login')->with('msg_success', 'Đăng xuất thành công');
    }

}
