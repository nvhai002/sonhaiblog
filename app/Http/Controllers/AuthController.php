<?php

namespace App\Http\Controllers;

use App\Http\Requests\ValidateRegister;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register()
    {
        return view('auth/register');
    }

    public function postRegister(ValidateRegister $request)
    {
        $data = [
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'phone' => $request->phone,
        ];
        $user = User::create($data);
        $user->roles()->attach(2);
        return redirect('signin')->with('msg_success', 'Đăng ký thành công !');
    }

    public function signin()
    {
        return view('auth/login');
    }

    public function postSignin(Request $request)
    {
        if (Auth::attempt([
                'username' => $request->credential,
                'password' => $request->password
            ]) || Auth::attempt(['email' => $request->credential, 'password' => $request->password])) {
            return redirect('')->with('msg_success', 'Đăng nhâp thành công');
        }
        return redirect('signin')->with('msg_fail', 'Thông tin đăng nhập chưa chính xác');
    }

    public function signout()
    {
        Auth::logout();
        return redirect('/');
    }
}
