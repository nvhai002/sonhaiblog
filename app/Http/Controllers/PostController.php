<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\ValidateEditPost;
use App\Http\Requests\ValidatePost;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{

    protected $post;
    protected $category;
    protected $tag;

    function __construct(Post $post, Category $category, Tag $tag)
    {
        $this->post = $post;
        $this->category = $category;
        $this->tag = $tag;
    }

    public function index()
    {
        $data = Post::where(['type' => 'post'])->get();

        return view('post/index', compact('data'));
    }

    public function getMyPost()
    {
        $data = $this->post->getListPostByOwner(Auth::user()->id);

        return view('post/index', compact('data'));
    }

    public function getPostDetail($id)
    {
        $data = $this->post->getPostById($id);

        return view('post/detail', compact('data'));
    }

    public function addPost()
    {
        $categoryArray = $this->category->getCategoryOption();

        return view('post/add_post', compact('categoryArray'));
    }

    public function postAddPost(ValidatePost $request)
    {
        $post = $this->post->createPost($request);
        if ($request->tags) {
            $this->post->checkTags($request->tags, $post->id);
        }
        return redirect('add-post');
    }

    public function editPost($id)
    {
        $data = $this->post->getPostById($id);
        $categoryArray = $this->category->getCategoryOption();
        $arrayTags = $this->post->getTagsByPost($id);
        return view('post/edit_post',
            compact('data', 'categoryArray', 'arrayTags'));
    }

    public function postEditPost($id, ValidateEditPost $request)
    {
        $this->post->updatePost($id, $request);

        $this->post->checkTags($request->tags, $id);

        return redirect('post');
    }

    public function delPost($id)
    {
        $this->post->delPost($id);
        return redirect("my-post");

    }

    public function getCategory(Request $request)
    {
        $categoryArray = $this->category->getCategoryOption();
        return response($categoryArray, 200);
    }


}
