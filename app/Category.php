<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = ['name', 'status'];

//    public function categoryItems(){
//        return $this->hasMany('App\SliderItem','slider_id','id');
//    }

    public function getCategoryOption()
    {
        $categoryOption = [];

        $listCategory = Self::select('id', 'name')->get();
        foreach ($listCategory as $value) {
            $categoryOption[$value->id] = $value->name;
        }

        return $categoryOption;
    }
}
